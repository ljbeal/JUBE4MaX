<?xml version="1.0" encoding="UTF-8"?>
<jube>
  <!-- Things needed as environment: account, binary path, lib paths -->

  <include-path>
    <!-- Machine specs -->
    <path tag="ariodante | hobsyllwin">${J4M_HOME}/platforms/ariodante</path>
    <path tag="vega+!gpu">${J4M_HOME}/platforms/sling/vegacpu</path>
    <path tag="vega+gpu">${J4M_HOME}/platforms/sling/vegagpu</path>
    <path tag="lumi+!gpu">${J4M_HOME}/platforms/lumi/lumi-cpu</path>
    <path tag="lumi+gpu">${J4M_HOME}/platforms/lumi/lumi-gpu</path>
    <path tag="karolina+!gpu">${J4M_HOME}/platforms/it4i/karolina-cpu</path>
    <path tag="karolina+gpu">${J4M_HOME}/platforms/it4i/karolina-gpu</path>
    <path tag="marenostrum+!gpu">${J4M_HOME}/platforms/marenostrum/MN5GPP</path>
    <path tag="marenostrum+gpu">${J4M_HOME}/platforms/marenostrum/MN5ACC</path>
    <path tag="leonardo+!gpu">${J4M_HOME}/platforms/cineca/leonardo/dcgp</path>
    <path tag="leonardo+gpu">${J4M_HOME}/platforms/cineca/leonardo/booster</path>
    <path tag="deucalion+!gpu">${J4M_HOME}/platforms/deucalion/deucalion-cpu</path>
    <path tag="deucalion+gpu">${J4M_HOME}/platforms/deucalion/deucalion-gpu</path>
    <path tag="meluxina+!gpu">${J4M_HOME}/platforms/meluxina/meluxina-cpu</path>
    <path tag="meluxina+gpu">${J4M_HOME}/platforms/meluxina/meluxina-gpu</path>
    <path tag="discoverer">${J4M_HOME}/platforms/discoverer</path>

    <!-- Deployment options (account,environment,etc) -->
    <path>${J4M_HOME}/applications/siesta/deployment</path>
    <path>${J4M_HOME}/applications/siesta/analysis</path>

    <!-- Workloads -->
    <path>${J4M_HOME}/workloads/siesta/covid-spike/</path>
  </include-path>

  <!-- This reads the default values from deploy.xml. Account should probably -->
  <!-- as an environment variable. -->
  <parameterset name="deployParameter" init_with="deploy.xml">
  </parameterset>

  <!-- Small Silicon Bands benchmark for small PCs -->
  <benchmark name="CovidWeak" outpath="_CovidWeak">

    <parameterset name="systemParameter" init_with="platform.xml">
      <!-- no. of nodes, tasks, etc -->
      <parameter name="nodes"          type="int" tag="!morenodes"> 1,2,4,8,16 </parameter>
      <parameter name="nodes"          type="int" tag="morenodes"> 32,64 </parameter>
      <parameter name="taskspernode"   type="int" >$default_ntasks</parameter>
      <parameter name="threadspertask" type="int" >$default_omp_threads</parameter>
    	<parameter name="ncpus"          type="int" mode="python">$taskspernode*$nodes*$threadspertask</parameter>
	    <parameter name="ncores"         type="int" mode="python">$taskspernode*$nodes*$threadspertask</parameter>
      <parameter name="executable" tag="!gpu">siesta</parameter>
      <parameter name="executable" tag="gpu">$gpubindfile siesta</parameter>
      <parameter name="args_executable"> $fin > $fout </parameter>
    </parameterset>

    <parameterset name="testcaseset" init_with="workload.xml">
      <parameter name="basis_file" mode="python">
      { "1"  :  "SZ.basis",
        "2"  :  "Custom1.basis",
        "4"  :  "Custom2.basis",
        "8"  :  "SZ.basis",
        "16" :  "Custom1.basis",
        "32" :  "Custom2.basis",
        "64" :  "SZ.basis"}["$nodes"]
      </parameter>
      <parameter name="supercell_x"    mode="python">
      { "1"  :  1.0,
        "2"  :  1.0,
        "4"  :  1.0,
        "8"  :  2.0,
        "16" :  2.0,
        "32" :  2.0,
        "64" :  4.0}["$nodes"]
      </parameter>
      <parameter name="tasks_per_pole" mode="python"> int($taskspernode / 4) *$nodes</parameter>
    </parameterset>

    <substituteset name="fdfsub" init_with="workload.xml">
    </substituteset>

    <!-- files needed incl job file and input files -->
    <fileset name="jobfiles" init_with="platform.xml">
      <copy tag="gpu"> ${J4M_HOME}/applications/siesta/gpu-bindings/$gpubindfile </copy>
      <copy>${J4M_HOME}/workloads/siesta/$dirin/*</copy>
    </fileset>

    <!-- Imports things needed from platform. -->
    <parameterset name="executeset" init_with="platform.xml">
      <parameter name="modules"> $environ_load  </parameter>
      <parameter name="wrappre"> $siestaexports </parameter>

      <parameter name="mapping" tag="deucalion+gpu" > --cpu-list $default_bind --bind-to cpu-list:ordered</parameter>
      <parameter name="args_starter" tag="deucalion+gpu" > -np $tasks $mapping </parameter>
    </parameterset>

    <substituteset name="executesub" init_with="platform.xml">
      <sub source="@COMMA" dest="," />
    </substituteset>

    <!-- Operation -->
    <step name="submit"  >
      <use>deployParameter</use>
      <use>testcaseset</use>
      <use>systemParameter</use>
      <use>executeset</use>
      <use>jobfiles</use>
      <use>executesub</use>
      <use>fdfsub</use>
      <do>$submit $submit_script</do> <!-- actual submission directive -->
    </step>

    <!-- Postprocessing analysis -->
    <patternset name="pattern" init_with="analysis.xml">
    </patternset>

    <analyser name="analyse">
      <use>pattern</use>
      <analyse step="submit">
        <file>time.json</file>
      </analyse>
    </analyser>

    <result>
      <use>analyse</use>
      <table name="result" style="csv" separator=";" transpose="False" sort="solution_method,elsi_solver,nodes">
        <column title="Nodes">nodes</column>
        <column title="Tasks/Node">taskspernode</column>
        <column title="Threads/Task">threadspertask</column>
        <column title="Solution Method">solution_method</column>
        <column title="ELSI Solver">elsi_solver</column>
        <column format=".2f" title="compute_dm">computedm</column>
        <column format=".2f" title="setup_H">setuph</column>
        <column format=".2f" title="walltime">walltime</column>
      </table>
    </result>
  </benchmark>
</jube>
