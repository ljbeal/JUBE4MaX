# IMPORTANT NOTICE

The siesta.xml file has been deprecated for the moment. Refer to the files
present under /max-inputs/interface/siesta for more info.

# Sample benchmarking runs for Siesta

The .xml files in this directory are examples of the use of JUBE to
organize benchmarking.

This is work in progress.

The main file `siesta.xml` is a template that contains a list of paths
encoding the location of all the available platform files and
workloads. Within the file one can specify the actual executable to be
used, as well as the number of nodes, tasks, etc. These values will
override the settings in the platform files. The `<benchmark>` element
is also here, so one could do, for example:

```
   jube run siesta.xml --tag marenostrum si-quantum-dot
```

Note that this approach seems to go against the JUBE modularity principle.
In fact, Yambo has a minimal `yambo.xml` application file, avoiding repetition
of other JUBE blocks.

Along the same line, it could be more appropriate to have
different main driver files for each actual benchmark.  For example,
we have `tests-ariodante.xml` for simple debugging tests in a laptop,
and `tests-mn.xml` for simple tests on MareNostrum. In the top level,
we have

```
  benchmark-siesta-qdot.xml
  profile-siesta-qdot.xml
```

for tests in Leonardo. There it is apparent that other MaX codes are
following the same route.

The executables, time limits, and number of nodes and tasks are
specified in the benchmark files. It makes no sense to use many tasks
in ariodante, or to use an ariodante module name in MareNostrum or
Leonardo. Perhaps one could set up other files to list available
executables, etc, but in a development setting this is not very
useful.

One should edit the main driver file to set the running parameters
for a given platform and workload.

```
   jube run tests-mn.xml --tag marenostrum si-quantum-dot
```

The `dry-run` tag can be used to inspect the `submit.job` files produced.

The drawback of this setup is that one has to maintain several almost
identical files. Perhaps other tricks can be used. In particular, absolute
machine-specific paths should be avoided as much as possible, maybe with
the use of environment variables (how?).

## Use of templated fdf files

In some of the tests some of the options in the fdf files have been
templated, so that they can be accessed by JUBE-run benchmarks. For
example, in `sih`, the MeshCutoff value can be set thus in the main
driver file:

```
    <!-- testcase parameters -->
    <parameterset name="testcaseset" init_with="workload.xml">
    <parameter name="meshcutoff">200,300,400</parameter>
    </parameterset>
```

and JUBE will run three different jobs with the different values.

This feature is enabled in all tests, but so far only
`si-quantum-dot`, `sih` and `water` have any options actually
templated.
