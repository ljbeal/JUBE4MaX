#!/bin/bash
CPU_ID=$(cat /proc/self/stat | awk '{print $39}')
rank="$SLURM_LOCALID"

limit0=20
limit1=40
limit2=60
limit3=80
limit4=100
limit5=120
limit6=140
limit7=160

if [ $CPU_ID -lt $limit0 ]
then
   export CUDA_VISIBLE_DEVICES=0

elif [ $CPU_ID -lt $limit1 ]
then
   export CUDA_VISIBLE_DEVICES=1

elif [ $CPU_ID -lt $limit2 ]
then
   export CUDA_VISIBLE_DEVICES=2

elif [ $CPU_ID -lt $limit3 ]
then
   export CUDA_VISIBLE_DEVICES=3

elif [ $CPU_ID -lt $limit4 ]
then
   export CUDA_VISIBLE_DEVICES=0

elif [ $CPU_ID -lt $limit5 ]
then
   export CUDA_VISIBLE_DEVICES=1

elif [ $CPU_ID -lt $limit6 ]
then
   export CUDA_VISIBLE_DEVICES=2

else
   export CUDA_VISIBLE_DEVICES=3

fi

echo "Task $rank CPU $CPU_ID GPU $CUDA_VISIBLE_DEVICES"
exec $*
