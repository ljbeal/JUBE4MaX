#!/bin/bash
CPU_ID=$(cat /proc/self/stat | awk '{print $39}')
rank="$SLURM_LOCALID"

limit0=8
limit1=16
limit2=24
limit3=32

if [ $CPU_ID -lt $limit0 ]
then
   export CUDA_VISIBLE_DEVICES=0

elif [ $CPU_ID -lt $limit1 ]
then
   export CUDA_VISIBLE_DEVICES=1

elif [ $CPU_ID -lt $limit2 ]
then
   export CUDA_VISIBLE_DEVICES=2

elif [ $CPU_ID -lt $limit3 ]
then
   export CUDA_VISIBLE_DEVICES=3

fi

echo "Task $rank Core $CPU_ID GPU $CUDA_VISIBLE_DEVICES"

exec $*
