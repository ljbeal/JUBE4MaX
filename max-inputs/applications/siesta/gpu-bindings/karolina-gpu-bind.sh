#!/bin/bash
CPU_ID=$(cat /proc/self/stat | awk '{print $39}')
rank="$SLURM_LOCALID"

limit0=24
limit1=32
limit2=56
limit3=64
limit4=88
limit5=96
limit6=120
limit7=128

if [ $CPU_ID -lt $limit0 ]
then
   export CUDA_VISIBLE_DEVICES=2

elif [ $CPU_ID -lt $limit1 ]
then
   export CUDA_VISIBLE_DEVICES=3

elif [ $CPU_ID -lt $limit2 ]
then
   export CUDA_VISIBLE_DEVICES=0

elif [ $CPU_ID -lt $limit3 ]
then
   export CUDA_VISIBLE_DEVICES=1

elif [ $CPU_ID -lt $limit4 ]
then
   export CUDA_VISIBLE_DEVICES=6

elif [ $CPU_ID -lt $limit5 ]
then
   export CUDA_VISIBLE_DEVICES=7

elif [ $CPU_ID -lt $limit6 ]
then
   export CUDA_VISIBLE_DEVICES=4

else
   export CUDA_VISIBLE_DEVICES=5
fi

echo "Task $rank Core $CPU_ID GPU $CUDA_VISIBLE_DEVICES"
exec $*
