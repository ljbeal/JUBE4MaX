def main():
    from BigDFT.Inputfiles import Inputfile
    from BigDFT.Systems import System
    from BigDFT.Fragments import Fragment
    from BigDFT.IO import XYZReader
    import yaml

    sys = System()
    with XYZReader("H2O") as ifile:
        sys["WAT:0"] = Fragment(xyzfile=ifile)

    sys.to_file("posinp.xyz")

    inp = Inputfile()
    inp.set_xc("PBE")

    with open("inp.yaml", "w+") as o:
        yaml.dump(dict(inp), o)


if __name__ == "__main__":
    main()
