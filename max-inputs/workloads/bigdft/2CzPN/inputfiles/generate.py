def main():
    from BigDFT.Inputfiles import Inputfile
    from BigDFT.Systems import System
    from BigDFT.Fragments import Fragment
    from BigDFT.IO import XYZReader, read_pdb
    import yaml

    with open("2CzPN_1.pdb") as ifile:
        sys = read_pdb(ifile)

    sys.to_file("posinp.xyz")

    inp = Inputfile()
    inp.set_xc("PBE")
    inp.set_hgrid(0.5)
    inp.set_rmult(coarse=5.0, fine=7.0)
    inp.set_psp_nlcc()

    with open("input.yaml", "w+") as o:
        yaml.dump(dict(inp), o)


if __name__ == "__main__":
    main()
