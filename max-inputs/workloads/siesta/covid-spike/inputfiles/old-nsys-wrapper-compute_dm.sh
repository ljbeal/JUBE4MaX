#!/bin/bash

if [[ $OMPI_COMM_WORLD_RANK == 0 || $OMPI_COMM_WORLD_RANK == 1 ]]; then
    nsys profile --trace=cuda,nvtx,mpi --capture-range=nvtx --nvtx-capture='compute_dm' \
         --nvtx-domain-include=default \
	 --capture-range-end=repeat[:1] --kill='none' \
         --env-var=NSYS_NVTX_PROFILER_REGISTER_ONLY=0,NSYS_MPI_STORE_TEAMS_PER_RANK=1  "$@"
else
        "$@"
fi
