This is a benchmark for Siesta based on a box of water molecules.
It uses the AiiDA framework (together with the aiiida-siesta package).

The 'water.py' script can be run with various command-line parameters that determine
the size of the calculation, the number of MPI processes, and the code and computer
details. Specifically:

usage: water.py [-h] -c CODENAME -b BASIS_ID [-n MPI_NODES] [-s NSIZE]
                [-e MESH_CUTOFF] [-r SEND_CALC]

Run the Water benchmark

optional arguments:
  -h, --help      show this help message and exit
  -c CODENAME     the name of the AiiDA code that references Siesta.siesta
                  plugin
  -b BASIS_ID     the code for basis set ("sz","dzp","tzp","qzdp") (default:
                  dzp)
  -n MPI_NODES    the number of MPI nodes (default: 2)
  -s NSIZE        the size of the supercell edges
  -e MESH_CUTOFF  the mesh cutoff in Rydbergs
  -r SEND_CALC    Whether to submit (run) after build test (0: no, 1: yes)
                  default:0 (dont-send)

The basic calculation (with -s 1) treats 192 atoms. Increasing the supercell size
allows the system to grow as required. A single parameter ( -s N ) controls all
three sizes of the supercell, so the enlargement grows as the cube of N.

A single-zeta (-b sz) basis set is the minimal one that can be
used. It corresponds to four orbitals O and just one for H. A
double-zeta basis set has 13 orbitals for O and 4 for H, and higher
qualities ("tzp", "qzdp") to even larger cardinalities.

The mesh-cutoff determines the fineness of the real-space grid. A
typical value is 300 Ry (-e 300).

PREPARATORY CONFIGURATION

* The aiida-siesta package.

  Download it from the GitHub releases page:
        https://github.com/albgar/aiida_siesta_plugin/releases
	
  Make sure you get at least version 0.11.7.
  
  Prepare a Python virtual environment, and install from the top level with

  pip install -e .

  This will pull in the appropriate version of aiida-core.
  
  Documentation for aiida-siesta can be found in:

       http://aiida-siesta-plugin.readthedocs.io/en/latest/   
       
* Codes

  Download the Siesta code (a specially patched 4.1 version) from:

   https://www.dropbox.com/s/erhwozomti83f98/siesta-4.1--848--json-time-6.tgz?dl=0
   
  Compile using an 'arch.make' file similar to the one provided in the Obj directory under
  the name 'parallel-arch.make'.

  sh ../Src/obj_setup.sh
  make
  (Copy the executable to some place, and use the 'verdi code setup' to add the code to
   the AiiDA database)

  NOTE: Make sure that you set 'export OMP_NUM_THREADS=1' as a pre-execution shell command if
  you are using an OpenMP enabled linear-algebra library but do not compile Siesta with OpenMP
  
* Pseudopotentials

  Before launching any calculation, a pseudopotential family must be uploaded to the AiiDA database.
  Each benchmark directory contains its own pseudopotential files. Upload with a command like:

  verdi data psf uploadfamily /path/to/water/psf_files Water "Psfs for DNA Benchmark"


RUNNING THE BENCHMARK

To test the setup, run, for example:

  ./water.py -c siesta-4.1-time@bigiron -n 16 -b dzp -s 2 -e 300 -r 0

and check that the 'submit_test' directory contains proper aiida.fdf and _aiida_submit.sh files.

To actually run a benchmark use '-r 1'

  ./water.py -c siesta-4.1-time@bigiron -n 16 -b dzp -s 3 -e 300 -r 1

You can use the script
aiida_siesta/examples/plugins/siesta/get_benchmark_info.py to extract
relevant size and timing information from the database after the
calculation finishes.

  ./get_benchmark_info.py {calc_id} | tee bench.dat

A set of lines is produced in stderr, and a summary line in stdout
which includes size information and timings. The timings are for the
total cpu time and for the most relevant sections: 'solver' and
'setup_Hamiltonian'.  The benchmark only exercises *one* scf step plus
a 'force' calculation, so one-time initialization and finalization
operations might dominate. In real usage, the number of scf steps will
be around 10-30, and the solver and setup_H will dominate the total
time. For scaling, they are indeed the most important.


* Appropriate values for the parameters

The following examples showcase the command line parameters, and the
output of the get_benchmark_info.py script. All calculations with 8 MPI processes.

With -s 1 and -b dzp, the number of orbitals is 1472:

./water.py -c siesta-4.1-time@jaguar-omp1 -b dzp -n 8 -s 1 -e 300 -r 1
832 1472 409478 [144, 144, 144] 2985984 8  'siesta-4.1-time'  jaguar-omp1  10.006 0.413 0.763

With -s 1 and -b tzp, the number of orbitals is 1856:

./water.py -c siesta-4.1-time@jaguar-omp1 -b tzp -n 8 -s 1 -e 300 -r 1
824 1856 1574102 [144, 144, 144] 2985984 8  'siesta-4.1-time'  jaguar-omp1  56.739 0.793 5.008

With -s 2 and -b tzp, the number of orbitals is eight times larger (14848)

Changing the mesh-cutoff to 500 Ryd ( -e 500 ) doubles the number of
mesh points. This should not affect the solver timing, but increase
the setup_H time (compare to first example):

./water.py -c siesta-4.1-time@jaguar-omp1 -b dzp -n 8 -s 1 -e 500 -r 1
840 1472 409478 [180, 180, 180] 5832000 8  'siesta-4.1-time'  jaguar-omp1  16.269 0.412 1.455

Larger systems can be immediately generated with the -s option. A
typical large value would be 120000 orbitals, obtained with '-s 4 -b
tzp'. Since the memory needs of the solver go as the square of the
number of orbitals, the number of MPI processes cannot be too small
for large systems.

Take care to review the "resources" section in the script if your scheduler demands it.
In particular, the line:

    calc.set_resources({"num_machines": 1, "num_mpiprocs_per_machine": args.mpi_nodes})

ALTERNATIVE TOPOLOGY

The script 'water-slab.py' is almost identical to 'water.py', except that vacuum is inserted
along the Z direction, turning the dense 3D box of water molecules into a 2D slab. This has
implications for the setup H part, and also for special solvers (such as PEXSI) whose performance
depends on the sparsity index and dimensionality of the system.

