# QE workloads

| case | workload  |
| ---- | --------- |
| CPU single-node  | ausurf |
| GPU single-node  | grir443   |
| GPU multi-node   | cnt10por8       |
| SINGLE-CORE | test-suite  |

## PW workloads

### ausurf112

- small system suitable for single node distribution with RG, up to 3 nodes with pools
- limited scaling on GPUs with RG ( 1-2 A100 64GB )

Two flavours:

1.ausurf112_qef
 - traditional ausurf test from [QEF/benchmarks](https://github.com/QEF/benchmarks/blob/master/AUSURF112/ausurf.in)
 - not full convergence, 2 scf steps, no disk io, forces and stresses 

2.ausurf112_25
 - full convergence, disk io default , forces and stresses

### cnt10por8_19
 - not full convergence, 16 steps, no disk io, scf
 - medium system suitable up to tens of gpus distributed with RG (GPUs), no pools

### cnt10porx
 - various workloads with increasing number of porphyrins for weak scaling
 - io active, full convergence

### cri3-1152
 - not full convergence, 3 steps, no disk io, scf
 - medium system suitable up to tens of nodes with R&G, scales up to 3 pools

### cri3-480
 - not full convergence, 3 steps, no disk io, scf
 - small system suitable for few nodes with R&G, scales up to 3 pools

### csi
 - full convergence in 25 steps, no disk io, scf
 - medium-large system with RG distribution intranode, scales up to 89 pools   

### grir443

Two flavours:

 - medium system with RG distribution up to few nodes, scales up to 4 pools
 - good for single-node GPU runs (4 A100 64 GPUs)
 - PAW

1.grir443
 - not full convergence, 6 steps, no disk io, scf only
 - performance for different scf steps might change, better represents production runs, but short enough to be profiled 

2.grir443_qef
 - traditional test from [QEF/benchmarks](https://github.com/QEF/benchmarks/blob/master/GRIR443/grir443.in)
 - not full convergence, 1 steps, no disk io, scf only

### si16l
 - full convergence, disk io
 - small system to test pool distribution and lr

### si32l 
 - full convergence, disk io
 - small system to test pool distribution and lr
 
## PH workloads

### si16l - 1 irrep
 - full lr simulation at selected q point, no disk io
 - pool scaling up to 128 

### si16l - all irrep
 - full lr simulation at selected q point, no disk io
 - pool scaling up to 128, image distribution up to 192

