#!/bin/bash
#from: Louis Stuber
#usage: srun ./binder.sh $0 ./a.out
#then ./memory-parser.sh $SLURM_JOBID
# Get MPI rank/size using SLURM and/or OpenMPI env variables.
# Edited by bellenlau - 22/09/2023
if [[ -z $OMPI_COMM_WORLD_LOCAL_RANK ]]; then
   export OMPI_COMM_WORLD_RANK=$SLURM_PROCID
   export OMPI_COMM_WORLD_LOCAL_RANK=$SLURM_LOCALID
   export OMPI_COMM_WORLD_SIZE=$SLURM_NTASKS
   export OMPI_COMM_WORLD_LOCAL_SIZE=$((OMPI_COMM_WORLD_SIZE/SLURM_NNODES))
fi
if [[ -z $OMPI_COMM_WORLD_LOCAL_RANK ]]; then
   export OMPI_COMM_WORLD_RANK=$PMI_RANK
   export OMPI_COMM_WORLD_LOCAL_RANK=$MPI_LOCALRANKID
   export OMPI_COMM_WORLD_SIZE=$PMI_SIZE
   export OMPI_COMM_WORLD_LOCAL_SIZE=$MPI_LOCALNRANKS
fi
if [[ -z $OMPI_COMM_WORLD_LOCAL_RANK ]]; then
   echo "$0 Error: OMPI_COMM_WORLD_LOCAL_RANK not defined. This script only supports SLURM and/or OpenMPI."
   exit 101
fi
if [[ -z $OMPI_COMM_WORLD_LOCAL_SIZE ]]; then
   if [[ -n $LSUB_NTASKS_PER_NODE ]]; then
      OMPI_COMM_WORLD_LOCAL_SIZE=$LSUB_NTASKS_PER_NODE
   else
      echo "$0 error: OMPI_COMM_WORLD_LOCAL_SIZE not defined."
      exit 102
   fi
fi
if [[ $OMPI_COMM_WORLD_LOCAL_RANK == 0 ]];then
   mkdir -p tmputil
   nvidia-smi --loop-ms=100 --format=csv --query-gpu=index,#EVENTS# > tmputil/tmpgpuutil.$SLURM_JOBID.$(hostname).txt &
   pid0=$!
fi
$@
if [[ $OMPI_COMM_WORLD_LOCAL_RANK == 0 ]];then
kill $pid0
fi
sleep 20
if [[ $OMPI_COMM_WORLD_RANK == 0 ]]; then
   for i in 0 1 2 3
   do
	   echo "#"$(sed -n '1p' tmputil/tmpgpuutil.$SLURM_JOBID.$(hostname).txt) > tmpgpuutil.$SLURM_JOBID.$(hostname).n$i.txt
        grep "^$i, " tmputil/tmpgpuutil.$SLURM_JOBID.$(hostname).txt >>  tmpgpuutil.$SLURM_JOBID.$(hostname).n$i.txt
   done
fi

