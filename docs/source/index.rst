JUBE ecosystem for MaX applications
===================================

This repository contains

* ``./max-inputs``: JUBE script to benchmark MaX applications on multiple platforms
* ``./examples`` : some examples to create JUBE scripts



.. note::

   This project is under active development.

Contents
--------

.. toctree::
   :maxdepth: 2

   general
   maxeco
